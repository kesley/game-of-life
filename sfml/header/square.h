#include <SFML/Graphics.hpp>
using sf::RectangleShape;
using sf::Vector2f;
using sf::Color;

#pragma once

class Square
{
private:
    
    RectangleShape m_square;

    int m_squareX;
    int m_squareY;


public:
    // Constructor
    Square(const Vector2f& l_size);
    
    // Getters
    Vector2f GetSquareSize() { return this->m_square.getSize(); }
    RectangleShape& GetSquare() { return this->m_square; }

    void ResetXPosition();
    void ResetYPosition();

    void IncreaseXPosition();
    void IncreaseYPosition();

    void UpdateSquare();

    // Setters
    void SetSquareColor(const Color& l_color);
};