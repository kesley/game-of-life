#include <SFML/Graphics.hpp>
using sf::Vector2u;

#include <vector>
using std::vector;

#pragma once

class Grid {

private:

  int m_qtdSquares;       // necessaria
  int m_qtdSquareWidth;   // necessaria
  
  vector<int> m_currentGen;
  vector<int> m_nextGen;

  int GetQtdNeighbours(int i);
  void ChangeGenerations();

public:

  Grid(const int& l_squareSize, const Vector2u& l_screenSize);

  // Getter
  int GetQtdSquares() const { return this->m_qtdSquares; }
  int GetArrayValue(const int& index) const { return this->m_currentGen[index]; }
  int GetQtdSquareWidth() const { return this->m_qtdSquareWidth; }


  void UpdateGrid();

};
