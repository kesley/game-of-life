#include <SFML/Graphics.hpp>
using sf::RenderWindow;
using sf::Vector2u;
using sf::Drawable;

#include <string>
using std::string;

#pragma once

class Window
{

private:
    void Setup(const string& l_title, const Vector2u& l_size, const int& l_framerate);

    void Destroy();
    void Create();

    RenderWindow m_window;
    Vector2u m_windowSize;
    string m_windowTitle;

    int m_windowFramerate;
    bool m_isDone;
    bool m_isFullscreen;


public:
    // Constructor
    Window(const std::string& l_title, const Vector2u l_size, const int& l_framerate);

    // Destructor
    ~Window();

    // Getters
    bool IsDone() { return this->m_isDone; };  
    bool IsFullscreen() { return this->m_isFullscreen; };
    Vector2u GetWindowSize() { return this->m_windowSize; };

    void Update();
    void BeginDraw();
    void Draw(Drawable& l_drawable);
    void EndDraw();
    void ToggleFullscreen();
};