#include "../header/window.h"

Window::Window(const string& l_title, const Vector2u l_size, const int& l_framerate)
{
    Setup(l_title, l_size, l_framerate);
}

Window::~Window()
{
    Destroy();
}

void Window::Setup(const string& l_title, const Vector2u& l_size, const int& l_framerate)
{
    this->m_windowTitle = l_title;
    this->m_windowSize = l_size;
    this->m_isFullscreen = false;
    this->m_isDone = false;
    this->m_windowFramerate = l_framerate;
    Create();
}

void Window::Create()
{
    auto style = (this->m_isFullscreen ? sf::Style::Fullscreen : sf::Style::Default);
    this->m_window.create({this->m_windowSize.x, this->m_windowSize.y, 32}, this->m_windowTitle, style);
    m_window.setFramerateLimit(this->m_windowFramerate);
}

void Window::Destroy()
{
    this->m_window.close();
}

void Window::Update()
{
    sf::Event event;

   while (m_window.pollEvent(event))
   {
       if (event.type == sf::Event::Closed)
       {
           this->m_isDone = true;
       }
       else if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::F5)
       {
           ToggleFullscreen();
       }
   } 
}

void Window::ToggleFullscreen()
{
    this->m_isFullscreen = !this->m_isFullscreen;
    Destroy();
    Create();
}

void Window::BeginDraw() 
{
    this->m_window.clear(sf::Color::White);
}

void Window::EndDraw()
{
    this->m_window.display();
}

void Window::Draw(Drawable& l_drawable)
{
    this->m_window.draw(l_drawable);
}
