#include <cstdlib>
using std::rand;

#include "../header/grid.h"

Grid::Grid(const int& l_squareSize, const Vector2u& l_screenSize)
{
  this->m_qtdSquareWidth = l_screenSize.x / l_squareSize;
  this->m_qtdSquares = (l_screenSize.x / l_squareSize) * (l_screenSize.y / l_squareSize);

  for (int i=0; i < this->m_qtdSquares; ++i)
  {
    this->m_currentGen.push_back(rand() % 2);
  }

}

int Grid::GetQtdNeighbours(int i)
{
  int l_qtdNeighbours = 0;

  // Validacao cruz

  // Validar cima
  if (i >= this->m_qtdSquareWidth)
  {
    if (this->m_currentGen[i-this->m_qtdSquareWidth] == 1) // verify up
    {
      l_qtdNeighbours++;
    }
  }

  // validar esquerda
  if (i-1 >= 0 && i % this->m_qtdSquareWidth != 0)
  {
    if (this->m_currentGen[i-1] == 1)
    {
      l_qtdNeighbours++;
    }
  }

  // Validar direita
  if (i+1 <= this->m_qtdSquares && (i+1) % this->m_qtdSquareWidth != 0)
  {
    if (this->m_currentGen[i+1] == 1)
    {
      l_qtdNeighbours++;
    }
  }

  // Validar baixo
  if (i+this->m_qtdSquareWidth <= this->m_qtdSquares)
  {
    if (this->m_currentGen[i+this->m_qtdSquareWidth] == 1)
    {
      l_qtdNeighbours++;
    }
  }

  // Validacao Diagonais

  // Validacao cima-esquerda
  if (i-1 >= 0 && i % this->m_qtdSquareWidth != 0 && i >= this->m_qtdSquareWidth)
  {
    if (this->m_currentGen[i-this->m_qtdSquareWidth-1] == 1)
    {
      l_qtdNeighbours++;
    }
  }

  // Validacao cima-direita
  if (i >= this->m_qtdSquareWidth && i+1 <= this->m_qtdSquares && (i+1) % this->m_qtdSquareWidth != 0)
  {
    if (this->m_currentGen[i-this->m_qtdSquareWidth+1])
    {
      l_qtdNeighbours++;
    }
  }

  // Validacao baixo-esquerda
  if(i+this->m_qtdSquareWidth <= this->m_qtdSquares && i-1 >= 0 && i % this->m_qtdSquareWidth != 0)
  {
    if (this->m_currentGen[i+this->m_qtdSquareWidth-1] == 1)
    {
      l_qtdNeighbours++;
    }
  }

  // validacao baixo-direita
  if(i+this->m_qtdSquareWidth <= this->m_qtdSquares && i+1 <= this->m_qtdSquares && (i+1) % this->m_qtdSquareWidth != 0)
  {
    if (this->m_currentGen[i+this->m_qtdSquareWidth+1] == 1)
    {
      l_qtdNeighbours++;
    }
  }

  return l_qtdNeighbours;
}

void Grid::ChangeGenerations()
{
  for (int i=0; i < this->GetQtdSquares(); ++i)
  {
    this->m_currentGen[i] = this->m_nextGen[i];
  }

  this->m_nextGen.clear();
}

void Grid::UpdateGrid()
{
  
  for (unsigned int i=0; i < this->m_qtdSquares; ++i)
  {
    
    int l_qtdNeighbours = GetQtdNeighbours(i);

    // Morto Vivo
    if (this->m_currentGen[i] == 1)
    {
      if (l_qtdNeighbours < 2)
      {
        this->m_nextGen.push_back(0);
      }
      else if (l_qtdNeighbours > 3)
      {
        this->m_nextGen.push_back(0);
      }
      else
      {
        this->m_nextGen.push_back(this->m_currentGen[i]);
      }
    }
    else
    {
      if (l_qtdNeighbours == 3)
      {
        this->m_nextGen.push_back(1);
      }
      else
      {
        this->m_nextGen.push_back(this->m_currentGen[i]);
      }
    }
  }

  ChangeGenerations();

}