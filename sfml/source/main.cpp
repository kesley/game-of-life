
#include "../header/grid.h"
#include "../header/window.h"
#include "../header/square.h"

int main()
{
  srand(time(NULL));

  Window window("Conway's game of life", Vector2u(1280, 720), 30);
  Square square(Vector2f(5.f, 5.f));
  Grid grid(square.GetSquareSize().x, window.GetWindowSize());

  while (!window.IsDone())
  {

    // Update
    window.Update();

    square.ResetXPosition();
    square.ResetYPosition();

    grid.UpdateGrid();

    // Draw
    window.BeginDraw();

    // Draw my stuff
    for (int i=0; i < grid.GetQtdSquares(); ++i)
    {

      if (i != 0)
      {
        square.IncreaseXPosition();

        if (i % grid.GetQtdSquareWidth() == 0)
        {
          square.ResetXPosition();
          square.IncreaseYPosition();
        }
      }

      square.UpdateSquare();

      if (grid.GetArrayValue(i) == 1)
      {
        window.Draw(square.GetSquare());
      }
    }

    window.EndDraw();
  }
  
  return 0;
}
