#include "../header/square.h"

Square::Square(const Vector2f& l_size)
{
    this->m_square.setSize(l_size);
    this->m_square.setFillColor(Color(0, 0, 0));
    
    this->m_square.setOutlineThickness(1.f);
    this->m_square.setOutlineColor(Color(255, 255, 255));

}

void Square::ResetXPosition()
{
    this->m_squareX = 0;
}

void Square::ResetYPosition()
{
    this->m_squareY = 0;
}

void Square::IncreaseXPosition()
{
    this->m_squareX += this->m_square.getSize().x;
}

void Square::IncreaseYPosition()
{
    this->m_squareY += this->m_square.getSize().y;
}

void Square::UpdateSquare()
{
    this->m_square.setPosition(this->m_squareX, this->m_squareY);
}