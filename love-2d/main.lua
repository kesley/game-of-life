function love.load()
	width, height = love.graphics.getDimensions()

	cellSize = 2
	cellsPopulation = {}

	for _ = 1, width / cellSize do
		local line = {}
		for _ = 1, height / cellSize do
			table.insert(line, love.math.random(0, 1))
		end
		table.insert(cellsPopulation, line)
	end

	love.graphics.setBackgroundColor(1, 1, 1)
	love.graphics.setColor(0, 0, 0)
end

function love.update()
	local next_frame = {}

	for i = 1, #cellsPopulation do
		local next_frame_line = {}

		for j = 1, #cellsPopulation[i] do
			local neighbourCounter = 0
			if i ~= 1 then
				if cellsPopulation[i - 1][j] == 1 then
					neighbourCounter = neighbourCounter + 1
				end
			end

			if i ~= #cellsPopulation then
				if cellsPopulation[i + 1][j] == 1 then
					neighbourCounter = neighbourCounter + 1
				end
			end

			if j ~= 1 then
				if cellsPopulation[i][j - 1] == 1 then
					neighbourCounter = neighbourCounter + 1
				end
			end

			if j ~= #cellsPopulation[i] then
				if cellsPopulation[i][j + 1] == 1 then
					neighbourCounter = neighbourCounter + 1
				end
			end

			if i ~= 1 and j ~= 1 then
				if cellsPopulation[i - 1][j - 1] == 1 then
					neighbourCounter = neighbourCounter + 1
				end
			end

			if i ~= 1 and j ~= #cellsPopulation[i] then
				if cellsPopulation[i - 1][j + 1] == 1 then
					neighbourCounter = neighbourCounter + 1
				end
			end

			if j ~= 1 and i ~= #cellsPopulation then
				if cellsPopulation[i + 1][j - 1] == 1 then
					neighbourCounter = neighbourCounter + 1
				end
			end

			if j ~= #cellsPopulation[i] and i ~= #cellsPopulation then
				if cellsPopulation[i + 1][j + 1] == 1 then
					neighbourCounter = neighbourCounter + 1
				end
			end

			if cellsPopulation[i][j] == 1 then
				if neighbourCounter < 2 or neighbourCounter > 3 then
					table.insert(next_frame_line, 0)
				else
					table.insert(next_frame_line, 1)
				end
			else
				if neighbourCounter == 3 then
					table.insert(next_frame_line, 1)
				else
					table.insert(next_frame_line, 0)
				end
			end
		end

		table.insert(next_frame, next_frame_line)
	end

	cellsPopulation = next_frame
end

function love.draw()
	for i = 1, #cellsPopulation do
		for j = 1, #cellsPopulation[i] do
			if cellsPopulation[i][j] == 1 then
				love.graphics.rectangle("fill", (i - 1) * cellSize, (j - 1) * cellSize, cellSize, cellSize)
			end
		end
	end
end
