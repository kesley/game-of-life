# Conway's game of life
This repo contains some implementations of a cellular automaton devised by the British mathematician John Horton Conway.

## Implementations
- lua
    - [love 2D](./love-2d)
- C++
    - [SFML](./sfml)
